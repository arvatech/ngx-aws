# NgxAws

Using AWS Cognito and AppSync GraphQL

## Setup

### window.global

Add this line to `./src/polyfills.ts` - needed by npm module buffer < v.5.0 that is added by webpack shim

    (window as any).global = window;

### HttpClientModule

Import `HttpClientModule` in the root module `./src/app/app.module.ts` like this:

    @NgModule({
      declarations: [
        ...
      ],
      imports: [
        ...
        HttpClientModule,
        ...
      ],
      providers: [
        ...
      ],
      bootstrap: [
        ...
      ],
    })
    export class AppModule { }

### Import Library

Add the local environment while importing in root module `./src/app/app.module.ts`:

    @NgModule({
      declarations: [
        ...
      ],
      imports: [
        ...
        NgxAwsModule.forRoot(environment),
        ...
      ],
      providers: [
        ...
      ],
      bootstrap: [
        ...
      ],
    })
    export class AppModule { }

---

Made by [ARVA Tech GmbH](https://arva.tech)
