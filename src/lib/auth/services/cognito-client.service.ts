import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AwsEnvironmentService } from '../../aws-environment.service';

@Injectable({
  providedIn: 'root'
})
export class CognitoClientService {

  private endpoint: string = `https://cognito-idp.${this.awsEnvironment.getUserPoolId().split('_')[0]}.amazonaws.com/`;

  constructor(
    private awsEnvironment: AwsEnvironmentService,
    private http: HttpClient,
  ) {
  }

  // a method that uses the HttpClient to send requests for all supported cognito operations
  request(operation, params, callback) {
    const options = {
      observe: 'response' as 'body',
      headers: new HttpHeaders({
        'Content-Type': 'application/x-amz-json-1.1',
        'X-Amz-Target': `AWSCognitoIdentityProviderService.${operation}`,
        'X-Amz-User-Agent': 'aws-arva',
      }),
    };

    this.http.post(this.endpoint, params, options)
      .subscribe((response: any) => {
        if (response.ok) {
          return callback(null, response.body);
        }
        const code = (response.body.__type || response.body.code).split('#').pop();
        const error = {
          code,
          name: code,
          message: (response.body.message || response.body.Message || null),
        };
        return callback(error);
      }, err => {
        // first check if we have a service error
        if (err && err.headers && err.headers.get('x-amzn-errortype')) {
          try {
            const code = (err.headers.get('x-amzn-errortype')).split(':')[0];
            const error = {
              code,
              name: code,
              statusCode: err.status,
              message: (err.error) ? err.error.message.toString() : null,
            };
            return callback(error);
          } catch (ex) {
            return callback(err);
          }

        } else if (err instanceof Error && err.message === 'Network error') {
          const error = {
            code: 'NetworkError',
            name: err.name,
            message: err.message,
          };
          return callback(error);
        } else {
          return callback(err);
        }
      });
  }
}
