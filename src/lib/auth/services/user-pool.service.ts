import { Injectable } from '@angular/core';
import { UserSessionService } from './user-session.service';
import { CookieService } from './cookie.service';
import { AwsEnvironmentService } from '../../aws-environment.service';

@Injectable({
  providedIn: 'root'
})
export class UserPoolService {

  clientId: string = this.awsEnvironment.getUserPoolWebClientId();
  userPoolId: string = this.awsEnvironment.getUserPoolId();

  constructor(
    private awsEnvironment: AwsEnvironmentService,
    private userSessionService: UserSessionService,
    private cookieService: CookieService,
  ) {
  }

  configure(data) {
    const { UserPoolId, ClientId } = data;
    if (!UserPoolId || !ClientId) {
      throw new Error('Both UserPoolId and ClientId are required.');
    }
    if (!/^[\w-]+_.+$/.test(UserPoolId)) {
      throw new Error('Invalid UserPoolId format.');
    }

    this.userPoolId = UserPoolId;
    this.clientId = ClientId;
  }

  getCurrentUser() {
    const idToken = this.userSessionService.getId(
      this.cookieService.get('arva.id'),
    );
    if (idToken) {
      const lastAuthUser = idToken.payload['cognito:username'];
      if (lastAuthUser) {
        return {
          username: lastAuthUser
        };
      }
      return null;
    }
    return null;
  }
}
