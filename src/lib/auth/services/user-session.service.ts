import { Injectable } from '@angular/core';
import { Buffer } from 'buffer';

@Injectable({
  providedIn: 'root'
})
export class UserSessionService {

  refreshToken: any;
  idToken: any;
  constructor() {

  }

  configure({ refreshToken, idToken }) {
    if (idToken == null) {
      throw new Error('Id Token must be present.');
    }

    this.refreshToken = refreshToken;
    this.idToken = idToken;
  }

  signOut() {
    this.refreshToken = undefined;
    this.idToken = undefined;
  }


  getRefreshToken() {
    return this.refreshToken;
  }

  getIdToken() {
    return this.idToken;
  }


  getInstance({ RefreshToken, IdToken }) {
    return {
      refreshToken: RefreshToken,
      idToken: IdToken
    };
  }

  getCurrent() {
    return {
      refreshToken: this.getRefreshToken(),
      idToken: this.getIdToken()
    };
  }

  getId(token) {
    return {
      jwtToken: token || '',
      payload: this.decodePayload(token),
    };
  }


  getRefresh(token) {
    return {
      jwtToken: token || ''
    };
  }


  decodePayload(token) {
    const payload = token.split('.')[1];
    try {
      return JSON.parse(Buffer.from(payload, 'base64').toString('utf8'));
    } catch (err) {
      return {};
    }
  }
}
