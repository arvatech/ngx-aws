// Inspired by https://github.com/7leads/ngx-cookie-service
// however, changed significantly by ARVA Tech GmbH
import { Injectable, Inject, PLATFORM_ID, InjectionToken } from '@angular/core';
import { DOCUMENT, isPlatformBrowser } from '@angular/common';
import { AwsEnvironmentService } from '../../aws-environment.service';

@Injectable({
  providedIn: 'root'
})
export class CookieService {

  private readonly documentIsAccessible: boolean;

  domain: string;
  secure: boolean;
  expires: number;

  constructor(
    private awsEnvironment: AwsEnvironmentService,
    @Inject(DOCUMENT) private document: any,
    @Inject(PLATFORM_ID) private platformId: InjectionToken<Object>,
  ) {
    this.documentIsAccessible = isPlatformBrowser(this.platformId);
    if (!this.documentIsAccessible) {
      return;
    }

    const url = new URL(document.URL);
    const prefix = this.awsEnvironment.getSubdomainPrefix();
    // so the tokens can be available for both shop and ves
    this.domain = url.hostname.indexOf(`${prefix}.`) === 0 ? url.hostname.replace(`${prefix}.`, '') : url.hostname;
    this.secure = url.protocol === 'https:' ? true : false;
  }


  check(name: string): boolean {
    if (!this.documentIsAccessible) {
      return false;
    }

    name = encodeURIComponent(name);

    const regExp: RegExp = this.getCookieRegExp(name);
    const exists: boolean = regExp.test(this.document.cookie);

    return exists;
  }


  get(name: string): string {
    if (this.documentIsAccessible && this.check(name)) {
      name = encodeURIComponent(name);

      const regExp: RegExp = this.getCookieRegExp(name);
      const result: RegExpExecArray = regExp.exec(this.document.cookie);

      return decodeURIComponent(result[1]);
    } else {
      return '';
    }
  }


  getAll(): {} {
    if (!this.documentIsAccessible) {
      return {};
    }

    const cookies: {} = {};
    const document: any = this.document;

    if (document.cookie && document.cookie !== '') {
      const split: Array<string> = document.cookie.split(';');

      for (let i = 0; i < split.length; i += 1) {
        const currentCookie: Array<string> = split[i].split('=');

        currentCookie[0] = currentCookie[0].replace(/^ /, '');
        cookies[decodeURIComponent(currentCookie[0])] = decodeURIComponent(currentCookie[1]);
      }
    }

    return cookies;
  }


  set(
    name: string,
    value: string,
  ): void {
    if (!this.documentIsAccessible) {
      return;
    }

    // first delete old cookie
    // this.set(name, '');

    this.document.cookie =
      encodeURIComponent(name) + '=' + encodeURIComponent(value) + ';'
      + 'expires=2147483647;'
      + 'path=/;'
      + 'domain=' + this.domain + ';'
      + (this.secure ? 'secure;' : '')
      + 'sameSite=lax;';
  }


  delete(name: string): void {
    if (!this.documentIsAccessible) {
      return;
    }

    this.set(name, '');
  }


  deleteAll(): void {
    if (!this.documentIsAccessible) {
      return;
    }

    const cookies: any = this.getAll();

    for (const cookieName in cookies) {
      if (cookies.hasOwnProperty(cookieName)) {
        this.delete(cookieName);
      }
    }
  }


  private getCookieRegExp(name: string): RegExp {
    const escapedName: string = name.replace(/([\[\]\{\}\(\)\|\=\;\+\?\,\.\*\^\$])/ig, '\\$1');

    return new RegExp('(?:^' + escapedName + '|;\\s*' + escapedName + ')=(.*?)(?:;|$)', 'g');
  }

}
