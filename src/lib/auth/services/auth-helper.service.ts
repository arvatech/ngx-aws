import { lib, SHA256, HmacSHA256 } from 'crypto-js';
import { Buffer } from 'buffer';
import BigInteger from '../helpers/BigInteger';
import { Injectable } from '@angular/core';
import { AwsEnvironmentService } from '../../aws-environment.service';

@Injectable({
  providedIn: 'root'
})
export class AuthHelperService {

  private initN = 'FFFFFFFFFFFFFFFFC90FDAA22168C234C4C6628B80DC1CD1'
    + '29024E088A67CC74020BBEA63B139B22514A08798E3404DD'
    + 'EF9519B3CD3A431B302B0A6DF25F14374FE1356D6D51C245'
    + 'E485B576625E7EC6F44C42E9A637ED6B0BFF5CB6F406B7ED'
    + 'EE386BFB5A899FA5AE9F24117C4B1FE649286651ECE45B3D'
    + 'C2007CB8A163BF0598DA48361C55D39A69163FA8FD24CF5F'
    + '83655D23DCA3AD961C62F356208552BB9ED529077096966D'
    + '670C354E4ABC9804F1746C08CA18217C32905E462E36CE3B'
    + 'E39E772C180E86039B2783A2EC07A28FB5C55DF06F4C52C9'
    + 'DE2BCBF6955817183995497CEA956AE515D2261898FA0510'
    + '15728E5A8AAAC42DAD33170D04507A33A85521ABDF1CBA64'
    + 'ECFB850458DBEF0A8AEA71575D060C7DB3970F85A6E1E4C7'
    + 'ABF5AE8CDB0933D71E8C94E04A25619DCEE3D2261AD2EE6B'
    + 'F12FFA06D98A0864D87602733EC86A64521F2B18177B200C'
    + 'BBE117577A615D6C770988C0BAD946E208E24FA074E5AB31'
    + '43DB5BFCE0FD108E4B82D120A93AD2CAFFFFFFFFFFFFFFFF';

  private N: BigInteger = new BigInteger(this.initN, 16);
  private g: BigInteger = new BigInteger('2', 16);
  private k: BigInteger;
  private smallAValue: BigInteger;
  private largeAValue: BigInteger;
  private UValue: BigInteger;
  private infoBits: Buffer = Buffer.from('Caldera Derived Key', 'utf8');
  private UHexHash: string;
  private poolName: string = this.awsEnvironment.getUserPoolId().split('_')[1];

  constructor(private awsEnvironment: AwsEnvironmentService) {
    this.k = new BigInteger(this.hexHash(`00${this.N.toString(16)}0${this.g.toString(16)}`), 16);
    this.smallAValue = this.generateRandomSmallA();
    this.getLargeAValue(() => { });
  }

  public getLargeAValue(callback) {
    if (this.largeAValue) {
      callback(null, this.largeAValue);
    } else {
      this.calculateA(this.smallAValue, (err, largeAValue) => {
        if (err) {
          callback(err, null);
        }

        this.largeAValue = largeAValue;
        callback(null, this.largeAValue);
      });
    }
  }

  private randomBytes(nBytes) {
    return Buffer.from((lib.WordArray.random(nBytes)).toString(), 'hex');
  }

  private generateRandomSmallA() {
    const hexRandom = this.randomBytes(128).toString('hex');

    const randomBigInt = new BigInteger(hexRandom, 16);
    const smallABigInt = randomBigInt.mod(this.N);

    return smallABigInt;
  }

  private calculateA(a, callback) {
    this.g.modPow(a, this.N, (err, A) => {
      if (err) {
        callback(err, null);
      }

      if (A.mod(this.N).equals(BigInteger.ZERO)) {
        callback(new Error('Illegal paramater. A mod N cannot be 0.'), null);
      }

      callback(null, A);
    });
  }

  private calculateU(A, B) {
    this.UHexHash = this.hexHash(this.padHex(A) + this.padHex(B));
    const finalU = new BigInteger(this.UHexHash, 16);

    return finalU;
  }

  private hash(buf) {
    const str = buf instanceof Buffer ? lib.WordArray.create(buf) : buf;
    const hashHex = SHA256(str).toString();

    return (new Array(64 - hashHex.length).join('0')) + hashHex;
  }

  private hexHash(hexStr) {
    return this.hash(Buffer.from(hexStr, 'hex'));
  }

  private computehkdf(ikm, salt) {
    const infoBitsWordArray = lib.WordArray.create(
      Buffer.concat([
        this.infoBits,
        Buffer.from(String.fromCharCode(1), 'utf8'),
      ])
    );
    const ikmWordArray = ikm instanceof Buffer ? lib.WordArray.create(ikm) : ikm;
    const saltWordArray = salt instanceof Buffer ? lib.WordArray.create(salt) : salt;

    const prk = HmacSHA256(ikmWordArray, saltWordArray);
    const hmac = HmacSHA256(infoBitsWordArray, prk);
    return Buffer.from(hmac.toString(), 'hex').slice(0, 16);
  }

  public getPasswordAuthenticationKey(username, password, serverBValue, salt, callback) {
    if (serverBValue.mod(this.N).equals(BigInteger.ZERO)) {
      throw new Error('B cannot be zero.');
    }

    this.UValue = this.calculateU(this.largeAValue, serverBValue);

    if (this.UValue.equals(BigInteger.ZERO)) {
      throw new Error('U cannot be zero.');
    }

    const usernamePassword = `${this.poolName}${username}:${password}`;
    const usernamePasswordHash = this.hash(usernamePassword);

    const xValue = new BigInteger(this.hexHash(this.padHex(salt) + usernamePasswordHash), 16);
    this.calculateS(xValue, serverBValue, (err, sValue) => {
      if (err) {
        callback(err, null);
      }

      const hkdf = this.computehkdf(
        Buffer.from(this.padHex(sValue), 'hex'),
        Buffer.from(this.padHex(this.UValue.toString(16)), 'hex'));

      callback(null, hkdf);
    });
  }

  private calculateS(xValue, serverBValue, callback) {
    this.g.modPow(xValue, this.N, (err, gModPowXN) => {
      if (err) {
        callback(err, null);
      }

      const intValue2 = serverBValue.subtract(this.k.multiply(gModPowXN));
      intValue2.modPow(
        this.smallAValue.add(this.UValue.multiply(xValue)),
        this.N,
        (err2, result) => {
          if (err2) {
            callback(err2, null);
          }

          callback(null, result.mod(this.N));
        }
      );
    });
  }

  private padHex(bigInt) {
    let hashStr = bigInt.toString(16);
    if (hashStr.length % 2 === 1) {
      hashStr = `0${hashStr}`;
    } else if ('89ABCDEFabcdef'.indexOf(hashStr[0]) !== -1) {
      hashStr = `00${hashStr}`;
    }
    return hashStr;
  }

}
