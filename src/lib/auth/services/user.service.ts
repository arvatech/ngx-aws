import { lib, enc, HmacSHA256 } from 'crypto-js';
import BigInteger from '../helpers/BigInteger';
import { Injectable } from '@angular/core';
import { UserSessionService } from './user-session.service';
import { CognitoClientService } from './cognito-client.service';
import { AuthHelperService } from './auth-helper.service';
import { Buffer } from 'buffer';
import { CookieService } from './cookie.service';
import { AwsEnvironmentService } from '../../aws-environment.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private monthNames =
    ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
  private weekNames = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];

  private keyPrefix = 'arva';

  username = '';

  private poolName: string = this.awsEnvironment.getUserPoolId().split('_')[1];
  private clientId: string = this.awsEnvironment.getUserPoolWebClientId();

  constructor(
    private awsEnvironment: AwsEnvironmentService,
    private userSessionService: UserSessionService,
    private cookieService: CookieService,
    private cognitoClientService: CognitoClientService,
    private authHelperService: AuthHelperService,
  ) {
  }

  // sends a post request to InitiateAuth
  // then authHelperService is used to generate the signature string for the provided password
  // then another request is sent to respond to the auth challenge PASSWORD_VERIFIER
  // response of the last request should contain the tokens so they are cached
  authenticateUser(authDetails, callback) {
    let serverBValue;
    let salt;
    const authParameters: any = {};

    authParameters.USERNAME = authDetails.Username;
    this.authHelperService.getLargeAValue((errOnAValue, aValue) => {
      if (errOnAValue) {
        authDetails.postMigrate ?
          callback.onMigrationFailure(errOnAValue) : callback.onFailure(authDetails, errOnAValue);
      }

      authParameters.SRP_A = aValue.toString(16);

      const jsonReq: any = {
        AuthFlow: 'USER_SRP_AUTH',
        ClientId: this.clientId,
        AuthParameters: authParameters,
      };

      this.cognitoClientService.request('InitiateAuth', jsonReq, (err, data) => {
        if (err) {
          return authDetails.postMigration ?
            callback.onMigrationFailure(err) : callback.onFailure(authDetails, err);
        }

        const challengeParameters = data.ChallengeParameters;

        this.username = challengeParameters.USER_ID_FOR_SRP;
        serverBValue = new BigInteger(challengeParameters.SRP_B, 16);
        salt = new BigInteger(challengeParameters.SALT, 16);

        this.authHelperService.getPasswordAuthenticationKey(
          this.username,
          authDetails.Password,
          serverBValue,
          salt,
          (errOnHkdf, hkdf) => {
            // getPasswordAuthenticationKey callback start
            if (errOnHkdf) {
              authDetails.postMigration ?
                callback.onMigrationFailure(errOnHkdf) : callback.onFailure(authDetails, errOnHkdf);
            }

            const dateNow = this.getNowString();

            const message = lib.WordArray.create(
              Buffer.concat([
                Buffer.from(this.poolName, 'utf8'),
                Buffer.from(this.username, 'utf8'),
                Buffer.from(challengeParameters.SECRET_BLOCK, 'base64'),
                Buffer.from(dateNow, 'utf8'),
              ])
            );
            const key = lib.WordArray.create(hkdf);
            const signatureString = enc.Base64.stringify(HmacSHA256(message, key));

            const challengeResponses: any = {};

            challengeResponses.USERNAME = this.username;
            challengeResponses.PASSWORD_CLAIM_SECRET_BLOCK = challengeParameters.SECRET_BLOCK;
            challengeResponses.TIMESTAMP = dateNow;
            challengeResponses.PASSWORD_CLAIM_SIGNATURE = signatureString;

            const respondToAuthChallenge = (challenge, challengeCallback) =>
              this.cognitoClientService.request('RespondToAuthChallenge', challenge,
                (errChallenge, dataChallenge) => {
                  if (errChallenge && errChallenge.code === 'ResourceNotFoundException' &&
                    errChallenge.message.toLowerCase().indexOf('device') !== -1) {
                    challengeResponses.DEVICE_KEY = null;
                    return respondToAuthChallenge(challenge, challengeCallback);
                  }
                  return challengeCallback(errChallenge, dataChallenge);
                });

            const jsonReqResp: any = {
              ChallengeName: 'PASSWORD_VERIFIER',
              ClientId: this.clientId,
              ChallengeResponses: challengeResponses,
              Session: data.Session,
            };
            respondToAuthChallenge(jsonReqResp, (errAuthenticate, dataAuthenticate) => {
              if (errAuthenticate) {
                return authDetails.postMigration ?
                  callback.onMigrationFailure(errAuthenticate) : callback.onFailure(authDetails, errAuthenticate);
              }

              const newSession = this.getCognitoUserSession(dataAuthenticate.AuthenticationResult);

              this.userSessionService.configure(newSession);
              this.cacheTokens();
              return callback.onSuccess();
            });
            return undefined;
          });
        return undefined;
      });
    });
  }


  getCognitoUserSession(authResult) {
    const refreshToken = this.userSessionService.getRefresh(authResult.RefreshToken);
    const idToken = this.userSessionService.getId(authResult.IdToken);

    const sessionData: any = {
      RefreshToken: refreshToken,
      IdToken: idToken
    };

    return this.userSessionService.getInstance(sessionData);
  }

  // if not valid try refreshing with the latest cookie data
  getSession(callback) {
    const refreshTokenKey = `${this.keyPrefix}.refresh`;
    const refreshToken = this.userSessionService.getRefresh(
      this.cookieService.get(refreshTokenKey),
    );

    if (!refreshToken.jwtToken) {
      return callback(new Error('Cannot retrieve a new session. Please authenticate.'), null);
    }
    this.refreshSession(refreshToken, callback);

    return undefined;
  }

  // refreshes the session by sending and validating the refresh token on the cognito endpoint
  refreshSession(refreshToken, callback) {
    const authParameters: any = {};
    authParameters.REFRESH_TOKEN = refreshToken.jwtToken;

    const jsonReq: any = {
      ClientId: this.clientId,
      AuthFlow: 'REFRESH_TOKEN_AUTH',
      AuthParameters: authParameters,
    };

    this.cognitoClientService.request('InitiateAuth', jsonReq, (err, authResult) => {
      if (err) {
        if (err.code === 'NotAuthorizedException') {
          this.clearCachedUser();
        }
        return callback(err, null);
      }
      if (authResult) {
        const authenticationResult = authResult.AuthenticationResult;
        if (!Object.prototype.hasOwnProperty.call(authenticationResult, 'RefreshToken')) {
          authenticationResult.RefreshToken = refreshToken.jwtToken;
        }
        this.userSessionService.configure(this.getCognitoUserSession(authenticationResult));
        this.cacheTokens();
        return callback(null, this.userSessionService.getCurrent());
      }
      return undefined;
    });
  }

  cacheTokens() {
    const refreshTokenKey = `${this.keyPrefix}.refresh`;
    const idTokenKey = `${this.keyPrefix}.id`;

    this.cookieService.set(refreshTokenKey, this.userSessionService.getRefreshToken().jwtToken);
    this.cookieService.set(idTokenKey, this.userSessionService.getIdToken().jwtToken);
  }

  clearCachedUser() {
    this.clearCachedTokens();
  }

  clearCachedTokens() {
    const refreshTokenKey = `${this.keyPrefix}.refresh`;
    const idTokenKey = `${this.keyPrefix}.id`;

    this.cookieService.delete(refreshTokenKey);
    this.cookieService.delete(idTokenKey);
  }

  signOut() {
    this.userSessionService.signOut();
    this.clearCachedUser();
  }

  getNowString() {
    const now = new Date();

    const weekDay = this.weekNames[now.getUTCDay()];
    const month = this.monthNames[now.getUTCMonth()];
    const day = now.getUTCDate();

    let hours: any = now.getUTCHours();
    if (hours < 10) {
      hours = `0${hours}`;
    }

    let minutes: any = now.getUTCMinutes();
    if (minutes < 10) {
      minutes = `0${minutes}`;
    }

    let seconds: any = now.getUTCSeconds();
    if (seconds < 10) {
      seconds = `0${seconds}`;
    }

    const year = now.getUTCFullYear();

    // ddd MMM D HH:mm:ss UTC YYYY
    const dateNow = `${weekDay} ${month} ${day} ${hours}:${minutes}:${seconds} UTC ${year}`;

    return dateNow;
  }

  public async getAuthorizationToken(): Promise<string> {
    return new Promise<string>(resolve => {
      this.getSession((err, session) => {
          if (err) {
            resolve();
          } else {
            resolve(this.userSessionService.getIdToken().jwtToken);
          }
        }
      );
    });
  }
}
