import { Injectable } from '@angular/core';
import { Store } from '@ngxs/store';
import { UserSessionService } from './services/user-session.service';
import { UserService } from './services/user.service';
import { UserPoolService } from './services/user-pool.service';
import { GraphQLService } from '../api/graphql.service';

class SignedOut {
  static readonly type = '[Auth] SignedOut';
  constructor(public payload: any) { }
}

class SignInSuccessful {
  static readonly type = '[Auth] SignInSuccessful';
  constructor(public payload: any) { }
}

class SignInFailed {
  static readonly type = '[Auth] SignInFailed';
  constructor(public payload: any) { }
}

@Injectable({
  providedIn: 'root'
})
export class CognitoService {

  private userAdminScope = 'aws.cognito.signin.user.admin';

  constructor(
    private store: Store,
    private userSessionService: UserSessionService,
    private userService: UserService,
    private userPoolService: UserPoolService,
    private graphqlService: GraphQLService
  ) {
  }

  // method used for signing in
  // calls the method from the user service
  public signIn(username: string, password: string): Promise<any> {
    const authDetails = {
      Username: username,
      Password: password,
      postMigration: false
    };
    return this.signInSRP(authDetails);
  }

  // method used for signing out
  // calls the method from the user service
  // dispatches an action
  public async signOut(): Promise<any> {
    const user = this.userPoolService.getCurrentUser();
    const token = this.userSessionService.getIdToken();
    if (user) {
      await this.userService.signOut();
    } else {
      console.log('no current Cognito user');
    }
    this.store.dispatch([new SignedOut({ ...user, token })]);
  }

  // gets info about the current user from the cookie
  public async currentAuthenticatedUser(): Promise<any> {
    try {
      const that = this;
      return new Promise((res, rej) => {
        // refresh the session if the session expired.
        this.userService.getSession((err, session) => {
          if (err) {
            rej(err);
            return;
          }

          const user = that.userPoolService.getCurrentUser();
          this.userService.username = user.username;
          if (!user) {
            rej('No current user');
            return;
          }

          // first try retrieving attributes from the cookie
          // if not send a request to Cognito
          if (err) {
              if (err.message === 'User is disabled' || err.message === 'User does not exist.') {
                rej(err);
              } else {
                res(user);
              }
              return;
            }

          const attributes = {
              email: session.idToken.payload.email
            };
          Object.assign(user, { attributes });
          return res({ ...user, token: this.userSessionService.getIdToken() });
        });
      });
    } catch (e) {
      throw new Error(('not authenticated'));
    }
  }

  // result of the method passed as a callback so the auth events can be handled with ngxs
  private authCallbacks(
    resolve: (value?: any) => void, reject: (value?: any) => void
  ): any {
    const that = this;
    return {
      onSuccess: async () => {
        try {
          const currentUser = await this.currentAuthenticatedUser();
          this.userService.username = currentUser.username;
          this.store.dispatch([new SignInSuccessful(
            currentUser
          )]);
          resolve(currentUser);
        } catch (e) {
          reject(e);
        }
      },
      onFailure: async (authDetails, err) => {
        console.log(err);
        await this.migrate(resolve, reject, authDetails);
      },
      onMigrationFailure: (err) => {
        this.store.dispatch([new SignInFailed(err)]);
        reject(err);
      }
    };
  }

  private signInSRP(authDetails: any) {
    if (!authDetails.Username) {
      return Promise.reject('Username cannot be empty');
    }
    // custom callbacks that dispatch actions to handle auth events
    return new Promise((resolve, reject) => {
      this.userService.authenticateUser(authDetails, this.authCallbacks(resolve, reject));
    });
  }

  private signInPostMigrate(resolve, reject, authDetails) {
    authDetails.postMigration = true;
    return this.userService.authenticateUser(authDetails, this.authCallbacks(resolve, reject));
  }

  private async migrate(resolve, reject, authDetails) {
    await this.graphqlService.graphql(`mutation PreLogin($input: MutationInputUser!) {
        preLogin(input: $input) {
          error
          data
        }
      }`, {
      input: {
        email: authDetails.Username,
        password: authDetails.Password
      }
    }).toPromise();
    await this.signInPostMigrate(resolve, reject, authDetails);
  }

}
