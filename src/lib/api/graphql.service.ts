import { Injectable } from '@angular/core';
import { Observable, from } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map, switchMap } from 'rxjs/operators';
import { AwsEnvironmentService } from '../aws-environment.service';
import { UserService } from '../auth/services/user.service';

export enum GraphQLAuthType {
  APIKEY,
  COGNITO,
}

export interface ListQuery {
  items: any[];
  nextToken: string;
}

export interface GraphQLResult {
  data: {
    [queryName: string]: {
      items?: any[];
      total: number;
    };
  };
  error?: any;
}

export interface GraphQLParams {
  queryName: string;
  fields: string;
  variables?: any;
  esDsl?: any;
  sort?: {
    field: string;
    direction: string;
  };
  size?: number;
  from?: number;
  authenticationType?: GraphQLAuthType;
  inputType?: string;
}

export const graphqlOperation = (query, variables = {}) => ({ query, variables });

@Injectable({
  providedIn: 'root'
})
export class GraphQLService {

  constructor(
    private awsEnvironment: AwsEnvironmentService,
    private userService: UserService,
    private http: HttpClient,
  ) {
  }

  public graphql(
    query: string,
    variables: Object = null,
    authenticationType: GraphQLAuthType = GraphQLAuthType.APIKEY,
  ): Observable<GraphQLResult> {
    return from(this.getHeaders(authenticationType)).pipe(
      switchMap(headers => this.http.post(
        this.awsEnvironment.getAppSyncUrl(),
        {
          query: query.replace(/\n/gm, ' '),
          variables,
        },
        {
          headers,
        },
      )),
      map(response => response as GraphQLResult),
    );
  }

  private async getHeaders(authenticationType: GraphQLAuthType = GraphQLAuthType.APIKEY): Promise<HttpHeaders> {

    let headers: HttpHeaders = new HttpHeaders({
      'Content-Type': 'application/json; charset=UTF-8',
    });

    if (authenticationType === GraphQLAuthType.APIKEY) {
      headers = headers.append('X-Api-Key', this.awsEnvironment.getAppSyncApiKey());
    } else if (authenticationType === GraphQLAuthType.COGNITO) {
      headers = headers.append('Authorization', await this.userService.getAuthorizationToken());
    }

    return headers;
  }

  list(model: string, fields: string, filter: any = null, limit: number = 50, nextToken: string = null): Observable<ListQuery> {
    console.error('use search instead');
    return null;
  }

  search(params: GraphQLParams): Observable<GraphQLResult> {
    params.variables = params.variables || {};
    if (params.esDsl) {
      params.variables.esDsl = JSON.stringify(params.esDsl);
    }
    if (params.sort) {
      params.variables.sort = params.sort;
    } else {
      params.variables.sort = { field: 'updatedAt', direction: 'desc' };
    }
    if (params.size) {
      params.variables.size = params.size;
    }
    if (params.from) {
      params.variables.from = params.from;
    }
    return this.graphql(
      `query a($esDsl:String $sort:SearchSortInput $size:Int $from:Int){
        ${params.queryName}(query:$esDsl sort:$sort size:$size from:$from){
          items{${params.fields}} total}}`,
      params.variables,
      params.authenticationType,
    );
  }

  getById(params: GraphQLParams): Observable<any> {
    return this.graphql(`query a($id:ID!){${params.queryName}(id:$id){${params.fields}}}`,
      params.variables,
      params.authenticationType,
    );
  }

  mutate(params: GraphQLParams): Observable<any> {
    this.emptyStringToNull(params.variables);
    return this.graphql(`mutation a($input:${params.inputType}!){${params.queryName}(input:$input){${params.fields}}}`,
      params.variables,
      params.authenticationType,
    );
  }

  // this method mutates the object
  emptyStringToNull(input: any): void {
    for (const i in input) {
      if (typeof input[i] === 'object') {
        this.emptyStringToNull(input[i]);
      } else if (typeof input[i] === 'string') {
        input[i] = input[i].trim();
        if (input[i] === '') {
          input[i] = null;
        }
      }
    }
  }

}
