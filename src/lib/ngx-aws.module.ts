import { ModuleWithProviders, NgModule } from '@angular/core';
import { AwsEnvironment } from './aws-environment.service';

@NgModule({
  declarations: [],
  imports: [
  ],
  exports: []
})
export class NgxAwsModule {
  static forRoot(config: AwsEnvironment): ModuleWithProviders {
    return {
      ngModule: NgxAwsModule,
      providers: [
        {provide: AwsEnvironment, useValue: config }
      ]
    };
  }
}


