import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AwsEnvironment {
   AWS_APPSYNC_API_URL: string;
   AWS_APPSYNC_API_KEY: string;
   AWS_COGNITO_USER_POOL_ID: string;
   AWS_COGNITO_USER_POOL_WEB_CLIENT_ID: string;
   SUBDOMAIN_PREFIX: string;
}


@Injectable({
  providedIn: 'root'
})
export class AwsEnvironmentService {
  private environment: AwsEnvironment;

  constructor(private config: AwsEnvironment) {
    if (config) { this.configure(config); }
  }

  configure(environment: AwsEnvironment) {
    this.environment = environment;
  }

  getAppSyncUrl() {
    return this.environment.AWS_APPSYNC_API_URL;
  }

  getAppSyncApiKey() {
    return this.environment.AWS_APPSYNC_API_KEY;
  }

  getUserPoolId() {
    return this.environment.AWS_COGNITO_USER_POOL_ID;
  }

  getUserPoolWebClientId() {
    return this.environment.AWS_COGNITO_USER_POOL_WEB_CLIENT_ID;
  }

  getSubdomainPrefix() {
    return this.environment.SUBDOMAIN_PREFIX;
  }


}
